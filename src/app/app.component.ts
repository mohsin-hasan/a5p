import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase';
import { environment } from '../environments/environment';
import { AuthService } from './_services/providers/auth.service';
import { ToasterService } from 'angular2-toaster';
import { LoaderService } from './_services/loader.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { CommonHelperService } from './_services/common-helper.service';

declare var $: any;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	title = 'app';
	isSignedIn = false;
	isLoading = false;
	loggedInUserInitials;
	user;

	constructor(
		private authService: AuthService,
		private _zone: NgZone,
		private toasterService: ToasterService,
		private loaderService: LoaderService,
		private router: Router,
		private angularFireAuth: AngularFireAuth,
		private commonHelperService: CommonHelperService
	) {
		this.loaderService.isLoading.subscribe(isLoading => {
			this.isLoading = isLoading;
		});

		this.commonHelperService.loggedInUserInitials.subscribe(loggedInUserInitials => {
			this.loggedInUserInitials = loggedInUserInitials;
		});
	}

	ngOnInit() {
		// setTimeout(() => {
			$(document).on('click', '.sign-out-btn', () => {
				return this.authService.signOut;
			});
		// }, 5000);
	}

	signOut() {
		// firebase.auth().signOut();
		console.log('signout');
	}
}
