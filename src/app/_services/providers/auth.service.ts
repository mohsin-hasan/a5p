import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { switchMap } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';

import { Post, Permissions } from '../../_interfaces/post';
import { User } from '../../_interfaces/user';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { LoaderService } from '../loader.service';
import { ToasterService } from 'angular2-toaster/src/toaster.service';
import { UUID } from 'angular2-uuid';




@Injectable()
export class AuthService {
	googleProvider = new firebase.auth.GoogleAuthProvider();
	authState = null;
	user: Observable<User>;

	constructor(
		private angularFireAuth: AngularFireAuth,
		private afs: AngularFirestore,
		private router: Router,
		private loaderService: LoaderService,
		private _zone: NgZone,
		private toasterService: ToasterService
	) {
		angularFireAuth.authState.subscribe(authState => {
			this.authState = authState;

			this.user = authState ? afs.doc<User>(`users/${authState.uid}`).valueChanges() : null;

		});
	}

	/**
	 * method to create new firebase user with email and password
	 * @param user User details containing Full name, email and password
	 */
	signUp(user) {
		this.loaderService.isLoading.next(true);

		this.angularFireAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
			.then((newUser) => {
				console.log('newUser : ', newUser);
				this._zone.run(() => {
					this.toasterService.pop('success', 'Congratulations !', 'Account successfully created');
				});

				newUser.updateProfile({ 'displayName': user.fullName })
					.then(() => {
						this.updateUserRoles(newUser)
							.then(() => {
								this.updatePostPermissions(newUser)
									.then(() => {
										this.router.navigate(['']);
										this.loaderService.isLoading.next(false);
									});
							});
					})
					.catch((error) => {
						console.log('error : ', error);
						this.toasterService.pop('error', 'Error', error.message);
						this.loaderService.isLoading.next(false);
					});
			})
			.catch((error) => {
				console.log('error : ', error);
				this.toasterService.pop('error', 'Error', error.message);
				this.loaderService.isLoading.next(false);
			});
	}

	/**
	 * Sign-in with Google account
	 */
	googleLogin() {
		this.loaderService.isLoading.next(true);
		return this.angularFireAuth.auth.signInWithPopup(this.googleProvider)
			.then((currentUser) => {
				this.isUserExist(currentUser.user.uid)
					.subscribe(isUserExist => {
						if (!isUserExist) {
							this.updateUserRoles(currentUser.user)
								.then(() => {
									this.updatePostPermissions(currentUser.user);
								});
						}
					});
				this.router.navigate(['/home']);
				this._zone.run(() => {
					this.toasterService.pop('success', 'Signed in as ', currentUser.user.displayName);
				});
			})
			.catch((error) => {
				this.loaderService.isLoading.next(false);
				this.toasterService.pop('error', 'Login Error ', error.message);
			});
	}

	/**
	 * sign-in with email and password
	 * @param email User provided email id
	 * @param password User provided password
	 */
	signInWithEmailAndPassword(email, password) {
		this.loaderService.isLoading.next(true);

		return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password)
			.then(loggedInUser => {
				this.isUserExist(loggedInUser.uid)
					.subscribe(isUserExist => {
						if (!isUserExist) {
							this.updateUserRoles(loggedInUser)
								.then(() => {
									this.updatePostPermissions(loggedInUser);
								});
						}
					});

				this.router.navigate(['/home']);
				this._zone.run(() => {
					this.toasterService.pop('success', 'Signed in as ', loggedInUser.displayName);
				});
			})
			.catch(error => {
				this.loaderService.isLoading.next(false);
				this.toasterService.pop('error', 'Error', error.message);
			});
	}

	/**
	 * method to send password reset email
	 * @param user User details containing Full name, email and password
	 */
	forgetPassword(user) {
		this.loaderService.isLoading.next(true);

		this.angularFireAuth.auth.sendPasswordResetEmail(user.email)
			.then((res) => {
				console.log('User : ', res);
				// tslint:disable-next-line:max-line-length
				this.toasterService.pop('info', 'Reset Password', 'A link has been sent to your email address. Please check your email to reset password');
				this.loaderService.isLoading.next(false);
			})
			.catch((error) => {
				console.log('error : ', error);
				this.toasterService.pop('error', 'Error', error.message);
				this.loaderService.isLoading.next(false);
			});
	}

	/**
	 * method to reset password
	 * @param newPassword new password for resetting user account
	 * @param mode email template action mode like its for resetPassword or for emailVerification
	 * @param oobCode code send to user's email to reset password
	 */
	resetPassword(newPassword, mode, oobCode) {
		this.loaderService.isLoading.next(true);
		this.angularFireAuth.auth.confirmPasswordReset(oobCode, newPassword)
			.then((result) => {
				console.log('Result : ', result);
				this.loaderService.isLoading.next(false);
				this.router.navigate(['']);
			})
			.catch((error) => {
				console.log('error : ', error);
				this.toasterService.pop('error', 'Error', error.message);
				this.loaderService.isLoading.next(false);
			});
	}

	/**
	 * Check for Existing User in FirebaseStore
	 * @param currentUserId uid of current logged-in user
	 */
	isUserExist(currentUserId) {
		let isUserExist = false;
		return this.afs.collection<User>('users')
			.valueChanges()
			.map(users => {
				if (users.length > 0) {
					_.each(users, (user) => {
						if (user.uid === currentUserId) {
							isUserExist = true;
							return false;
						}
					});
				}
				return isUserExist;
			});
	}

	/**adding new user to firebase 'Users' collections with updated default roles.
	 * default roles is 'Reader' for new user
	 * @param user current logged-in user
	 * */
	updateUserRoles(user) {
		// Sets user data to firestore on login
		const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
		const data: User = {
			uid: user.uid,
			email: user.email,
			displayName: user.displayName,
			roles: {
				reader: true,
				editor: false,
				admin: false
			}
			// roles: [{ reader: true }, { admin: false }, { editor: false }]
			// roles: ['admin', 'reader', 'editor']
		};

		return userRef.set(data, { merge: true });
	}

	/**
	 * Updating Posts permissions for new added user.
	 * Given only read permission to all new user by default
	 * @param user current logged-in user
	 */
	updatePostPermissions(user) {
		const Posts = this.afs.collection<Post>('post');

		return Posts.ref.get().then(post => {
			// console.log('clubs snapshot: ', post.docs);
			_.each(post.docs, (item) => {
				const permissions = [];
				let isUserExist = false;

				if (item.data().permissions) {
					// permissions.concat(item.data().permissions);
					_.each(item.data().permissions, permission => {
						permissions.push(permission);
					});
					_.each(permissions, permission => {
						// permissions.push(permission);
						if (permission.uid === user.uid) {
							isUserExist = true;
						}
					});
				}
				if (!isUserExist) {
					permissions.push({
						uid: user.uid,
						email: user.email,
						// roles: ['reader']
						roles: {
							reader: true,
							editor: false,
							admin: false
						}
					});
				}

				const itemData = {
					permissions: permissions
				};

				item.ref.set(itemData, { merge: true });
			});
		});
	}

	addPost(user, post) {
		const uid = UUID.UUID();
		const postRef: AngularFirestoreDocument<any> = this.afs.doc(`post/${uid}`);
		const data: Post = {
			title: post.title,
			content: post.content,
			createdAt: new Date(),
			permissions: [{
				uid: user.uid,
				email: user.email,
				roles: {
					reader: true,
					editor: true,
					admin: true
				}
			}],
			author: {
				uid: user.uid,
				email: user.email,
				name: user.displayName
			}
		};

		return postRef.set(data, { merge: true });
	}

	// Returns true if user is logged in
	get authenticated(): boolean {
		return this.authState !== null;
	}

	// Returns current user UID
	get currentUser(): string {
		return this.authenticated ? this.authState.displayName : null;
	}

	get signOut() {
		this.router.navigate(['']);
		return this.angularFireAuth.auth.signOut();
	}

	canEdit(user, post) {
		const allowedUser = ['admin', 'editor'];
		return this.checkAuthorization(user, post, allowedUser);
	}

	canDelete(user, post) {
		const allowedUser = ['admin'];
		return this.checkAuthorization(user, post, allowedUser);
	}

	checkAuthorization(user, post, allowedUser) {

		/**
		 * If post is posted by the current user then he/she have edit and delete pemission regardless of
		 * his/her user's role and post permission.
		 * A User by default have all three permission i.e. Reader, Editor and Admin on the post he created.
		 */
		if (user.uid === post.author.uid) {
			return true;
		}

		for (const role of allowedUser) {
			if (user.roles[role]) {
				// _.each(post.permissions, permission => {
				for (const permission of post.permissions) {
					if (permission.uid === user.uid) {
						if (permission.roles[role]) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
