import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LoaderService {
	isLoading = new BehaviorSubject(false);

	constructor() { }

}
