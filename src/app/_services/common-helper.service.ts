import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class CommonHelperService {
	loggedInUserInitials = new BehaviorSubject('');
	constructor() { }

	/**
   * Method set Logged-in user's name initials.
   */
	setLoggedInUserInitials(user) {
		const loggedInUserInitials = user.displayName.split(' ').map((n) => n[0]).join('');
		console.log(loggedInUserInitials);
		this.loggedInUserInitials.next(loggedInUserInitials);
		// return null;
	}

}
