export interface Post {
	title: string;
	content: string;
	createdAt: Date;
	permissions: Permissions[];
	author: Author;
	lastEditedBy?: LastEditedBy;
}

export interface Permissions {
	uid: string;
	email: string;
	roles: Roles;
	like?: boolean;
}

export interface Roles {
	reader?: boolean;
	editor?: boolean;
	admin?: boolean;
}

export interface Author {
	uid: string;
	email: string;
	name: string;
}

export interface LastEditedBy {
	uid: string;
	email: string;
	displayName: string;
}
