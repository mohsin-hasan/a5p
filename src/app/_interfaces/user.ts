export interface User {
	uid: string;
	email: string;
	displayName: string;
	roles: Roles;
}
export interface Roles {
	reader?: boolean;
	editor?: boolean;
	admin?: boolean;
}

// export interface User {
// 	uid: string;
// 	email: string;
// 	roles: Roles[];
// }

// export interface Roles {
// 	roles: string[];
// }
