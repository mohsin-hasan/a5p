import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { SignupComponent } from './signup/signup.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UploadComponent } from './upload/upload.component';

const routes: Routes = [
	{ path: '', component: LoginComponent, canActivate: [AuthGuard] },
	// { path: '?:mode/:oobCode/:apiKey', component: LoginComponent },
	{ path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
	{ path: 'signUp', component: SignupComponent },
	{ path: 'forget-password', component: ForgetPasswordComponent },
	{ path: 'reset-password', component: ResetPasswordComponent },
	{ path: 'upload', component: UploadComponent },
	{ path: '**', redirectTo: '' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
