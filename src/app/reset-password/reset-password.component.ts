import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../_services/providers/auth.service';
import { LoaderService } from '../_services/loader.service';

@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
	mode: string;
	oobCode: string;
	user: any = {};
	isLoading = false;

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private authService: AuthService,
		private	loaderService: LoaderService
	) {
		this.loaderService.isLoading.subscribe(isLoading => {
			this.isLoading = isLoading;
		});
	}

	ngOnInit() {
		this.activatedRoute.queryParams.subscribe((params) => {
			this.oobCode = params.oobCode;
			this.mode = params.mode;

			console.log(this.oobCode + ' ' + this.mode);
		});
	}

}
