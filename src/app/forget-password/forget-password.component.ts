import { Component, OnInit } from '@angular/core';

import { LoaderService } from '../_services/loader.service';
import { Router } from '@angular/router';
import { AuthService } from '../_services/providers/auth.service';

@Component({
	selector: 'app-forget-password',
	templateUrl: './forget-password.component.html',
	styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
	user: any = {};
	isLoading = false;

	constructor(
		private loaderService: LoaderService,
		private authService: AuthService
	) {
		this.loaderService.isLoading.subscribe(isLoading => {
			this.isLoading = isLoading;
		});
	}

	ngOnInit() {
	}

}
