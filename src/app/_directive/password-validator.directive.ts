import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';

@Directive({
	selector: '[appPasswordValidator][formControlName],[appPasswordValidator][formControl],[appPasswordValidator][ngModel]',
	providers: [
		{ provide: NG_VALIDATORS, useExisting: forwardRef(() => PasswordValidatorDirective), multi: true }
	]
})
export class PasswordValidatorDirective implements Validator {

	constructor(
		@Attribute('appPasswordValidator') public confirmPassword: string,
		@Attribute('reverse') public reverse: string
	) { }

	private get isReverse() {
		if (!this.reverse) {
			return false;
		}

		return this.reverse === 'true' ? true : false;
	}

	validate(c: AbstractControl): { [key: string]: any } {
		const v = c.value;

		const e = c.root.get(this.confirmPassword);

		if (e && v !== e.value && !this.isReverse) {
			return { confirmPassword: false }
		}

		if (e && v !== e.value && this.isReverse) {
			e.setErrors({ confirmPassword: false });
		}

		if (e && v === e.value && this.isReverse) {
			delete e.errors['confirmPassword'];
			if (!Object.keys(e.errors).length) {
				e.setErrors(null);
			}
		}
		return null;
	}
}
