import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../_services/providers/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(
		private router: Router,
		private authService: AuthService,
		private angularFireAuth: AngularFireAuth) { }
	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		// if (this.authService.user) {
		// 	return true;
		// }
		return this.angularFireAuth.authState.map(authState => {
			if (authState) {
				if (state.url === '/' || state.url === '') {
					this.router.navigate(['/home']);
					return false;
				}
				return true;
			} else if (state.url === '/' || state.url === '') {
				return true;
			} else {
				this.router.navigate(['']);
				return false;
			}
		});
	}
}
