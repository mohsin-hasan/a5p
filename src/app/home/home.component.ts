import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../_services/providers/auth.service';
import { LoaderService } from '../_services/loader.service';

import { Popover, PopoverContent } from 'ng2-popover';
import { ToasterService } from 'angular2-toaster/src/toaster.service';
import { NgForm } from '@angular/forms';
// import * as $ from 'jquery';
declare var $: any;
import * as _ from 'lodash';
import { CommonHelperService } from '../_services/common-helper.service';
import * as firebase from 'firebase';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	posts = [];
	user;
	isEditMode = false;
	post: any = {};
	isWritePost = false;
	isLoading = false;
	loggedInUserInitials;

	@ViewChild('addPostForm') addPostForm: NgForm;

	constructor(
		private afs: AngularFirestore,
		private authService: AuthService,
		private loaderService: LoaderService,
		private toasterService: ToasterService,
		private commonHelperService: CommonHelperService) {
		if (this.authService.user) {
			this.authService.user.subscribe(user => {
				console.log('user : ', user);
				this.user = user;
				this.commonHelperService.setLoggedInUserInitials(this.user);
			});
		}
		this.loaderService.isLoading.subscribe(isLoading => {
			this.isLoading = isLoading;
		});

		this.loaderService.isLoading.next(true);
	}

	ngOnInit() {
		if (this.authService.authenticated) {
			const user = this.authService.authState;
			console.log('user : ', user);
			$(document).ready(function () {
				$('[data-toggle="popover"]').popover({
					// title: 'Custom title',
					html: true,
					content: `<div class="col-sm-12 row sign-out-container">
							<div class="col-sm-9 display-name">${user.displayName}
								<small class="text-muted">${user.email}</small>
							</div>
							<div class="col-sm-3 sign-out-btn">
								<span>Sign Out</span>
							</div>
						</div>`,
					trigger: 'focus'
				});
			});
		}

		this.loadAllPosts();
	}

	/**
	 * method to fetch all the Posts entries from the Firebase store on page load
	 */
	loadAllPosts() {
		this.afs.collection('post').ref
			.get()
			.then((querySnapshot) => {
				this.posts = [];
				const query = querySnapshot.query.orderBy('createdAt', 'desc');

				query.get()
					.then((docsQuerySnapshot) => {
						console.log('docsQuerySnapshot :', docsQuerySnapshot);
						// docsQuerySnapshot.forEach((doc) => {
						_.each(docsQuerySnapshot.docs, (doc) => {
							console.log('doc', doc);
							const post = doc.data();
							post.docTitle = doc.id;
							post.isEditable = this.authService.canEdit(this.user, doc.data());
							post.isDelete = this.authService.canDelete(this.user, doc.data());
							post.isEditMode = false;
							if (!post.like) {
								post.like = 0;
							}
							if (!post.unlike) {
								post.unlike = 0;
							}
							const currentUserLikeUnlike = _.find(post.permissions, (permission) => permission.uid === this.user.uid);
							post.postLiked = _.isUndefined(currentUserLikeUnlike) ? currentUserLikeUnlike : currentUserLikeUnlike.like;
							this.posts.push(post);
						});
						this.loaderService.isLoading.next(false);

						// Initializing bootstrap tooltip
						$(document).ready(function () {
							$('[data-toggle=tooltip]').tooltip();
						});
					})
					.catch((error) => {
						console.log('error : ', error);
						this.toasterService.pop('error', 'Error', error.message);
						this.loaderService.isLoading.next(false);
					});
			})
			.catch((error) => {
				console.log('Error getting documents: : ', error);
				this.toasterService.pop('error', 'Error', error.message);
				this.loaderService.isLoading.next(false);
			});
	}

	/**
	 * method to save edited changes of particular Post into Firebase store
	 * @param post Edited post
	 */
	saveEditedPost(post) {
		this.loaderService.isLoading.next(true);
		this.afs.doc('post/' + post.docTitle).update({
			content: post.content,
			lastEditedBy: {
				uid: this.user.uid,
				displayName: this.user.displayName,
				email: this.user.email
			}
		})
			.then(() => {
				this.loaderService.isLoading.next(false);
			})
			.catch((error) => {
				console.log('error : ', error);
				this.toasterService.pop('error', 'Error', error.message);
				this.loaderService.isLoading.next(false);
			});

		this.ngOnInit();
	}

	/**
	 * method to permanently delete the post from Firebase store
	 * @param post Post to delete
	 */
	deletePost(post) {
		this.loaderService.isLoading.next(true);

		this.afs.doc('post/' + post.docTitle).delete()
			.then(() => {
				this.toasterService.pop('success', '', 'Post deleted !');
				this.ngOnInit();
				this.loaderService.isLoading.next(false);
			})
			.catch((error) => {
				console.log('error : ', error);
				this.toasterService.pop('error', 'Error', error.message);
				this.loaderService.isLoading.next(false);
			});
	}

	/**
	 * method to create new Post entry into Firebase store
	 */
	addPost() {
		if (this.isWritePost) {
			this.loaderService.isLoading.next(true);
			this.authService.addPost(this.user, this.post)
				.then(() => {
					this.toasterService.pop('success', '', 'New Post Added !');
					this.ngOnInit();
				})
				.catch((error) => {
					console.log('error : ', error);
					this.toasterService.pop('error', 'Error', error.message);
					this.loaderService.isLoading.next(false);
				});
		}
		this.isWritePost = this.isWritePost ? false : true;
		if (this.addPostForm) {
			this.addPostForm.reset();
		}
	}

	/**
	 * method to Like/Unlike the post specific to the logged-in user
	 * @param post post which have to like/unlike
	 * @param key key which decide whether have to Liked or Unliked by the user.
	 */
	likeOrUnlikePost(post, key) {
		let isUserPermissionExist = false;
		let userPermissions;
		let index;
		const permissions = [];
		let firstTimeLikeUnlike: boolean;
		switch (key) {
			case 'like':
				_.each(post.permissions, (userPermission, permissionIndex) => {
					permissions.push(userPermission);
					if (userPermission.uid === this.user.uid) {
						index = permissionIndex;
						isUserPermissionExist = true;
						userPermissions = userPermission;
					}
				});

				if (isUserPermissionExist && (_.isUndefined(userPermissions.like) || !userPermissions.like)) {
					firstTimeLikeUnlike = _.isUndefined(userPermissions.like);
					userPermissions.like = true;
					if (index > -1) {
						permissions[index] = userPermissions;
					}

					this.incrementLikeUnlikeCount(post, permissions, 'like', firstTimeLikeUnlike);

				} else if (!isUserPermissionExist) {
					permissions.push({
						uid: this.user.uid,
						email: this.user.email,
						roles: {
							reader: true,
							editor: false,
							admin: false
						},
						like: true
					});

					this.incrementLikeUnlikeCount(post, permissions, 'like', true);
				}
				break;

			case 'unlike':
				_.each(post.permissions, (userPermission, permissionIndex) => {
					permissions.push(userPermission);
					if (userPermission.uid === this.user.uid) {
						index = permissionIndex;
						isUserPermissionExist = true;
						userPermissions = userPermission;
					}
				});

				if (isUserPermissionExist && (_.isUndefined(userPermissions.like) || userPermissions.like)) {
					firstTimeLikeUnlike = _.isUndefined(userPermissions.like);
					userPermissions.like = false;
					if (index > -1) {
						permissions[index] = userPermissions;
					}

					this.incrementLikeUnlikeCount(post, permissions, 'unlike', firstTimeLikeUnlike);

				} else if (!isUserPermissionExist) {
					permissions.push({
						uid: this.user.uid,
						email: this.user.email,
						roles: {
							reader: true,
							editor: false,
							admin: false
						},
						like: false
					});

					this.incrementLikeUnlikeCount(post, permissions, 'unlike', true);
				}
				break;
		}
	}

	/**
	 * checks post's like or unlike for current logged-in user whether he like/Unlike the post
	 * @param post specfic post for whick we check like/unlike for current logged-in user
	 */
	checkPostLikeUnlike(post) {
		// if (this.user.uid === post.author.uid) {
		return true;
		// }
	}

	/**
	 * method toh icrement like/unlike count and to decrement unlike/like count of the post
	 * @param key key to decide whether its like increment of unlike increment
	 */
	incrementLikeUnlikeCount(post, permissions, key, firstTimeLikeUnlike) {
		let data;
		// let likeCount = 0;
		// let unlikeCount = 0;

		switch (key) {
			case 'like':
				let likeCount = 0;
				if (post.like) {
					likeCount = post.like;
				}
				if (_.isUndefined(post.unlike)) {
					post.unlike = 0;
				}
				data = {
					permissions: permissions,
					like: ++likeCount,
					unlike: firstTimeLikeUnlike || post.unlike === 0 ? post.unlike : --post.unlike
				};
				this.afs.doc('post/' + post.docTitle).set(data, { merge: true });

				// this.posts.forEach((item) => {
				_.each(this.posts, (item) => {
					if (item.docTitle === post.docTitle) {
						item.like = likeCount;
						item.unlike = post.unlike;

						const currentUserLikeUnlike = _.find(permissions, (permission) => permission.uid === this.user.uid);
						item.postLiked = _.isUndefined(currentUserLikeUnlike) ? currentUserLikeUnlike : currentUserLikeUnlike.like;
					}
				});
				break;

			case 'unlike':
				let unlikeCount = 0;
				if (post.unlike) {
					unlikeCount = post.unlike;
				}
				if (_.isUndefined(post.like)) {
					post.like = 0;
				}
				data = {
					permissions: permissions,
					like: firstTimeLikeUnlike || post.like === 0 ? post.like : --post.like,
					unlike: ++unlikeCount
				};
				this.afs.doc('post/' + post.docTitle).set(data, { merge: true });

				// this.posts.forEach((item) => {
				_.each(this.posts, (item) => {
					if (item.docTitle === post.docTitle) {
						item.like = post.like;
						item.unlike = unlikeCount;

						const currentUserLikeUnlike = _.find(permissions, (permission) => permission.uid === this.user.uid);
						item.postLiked = _.isUndefined(currentUserLikeUnlike) ? currentUserLikeUnlike : currentUserLikeUnlike.like;
					}
				});
				break;
		}
	}
}
