import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TooltipModule } from 'ng2-tooltip';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { ToasterModule } from 'angular2-toaster';
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';
import { Ng2FileInputModule } from 'ng2-file-input';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { AuthService } from './_services/providers/auth.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LoaderService } from './_services/loader.service';
import { AuthGuard } from './_guards/auth.guard';
import { SignupComponent } from './signup/signup.component';
import { PasswordValidatorDirective } from './_directive/password-validator.directive';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { CommonHelperService } from './_services/common-helper.service';
import { UploadComponent } from './upload/upload.component';


@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		LoginComponent,
		SignupComponent,
		PasswordValidatorDirective,
		ForgetPasswordComponent,
		ResetPasswordComponent,
		UploadComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		AppRoutingModule,
		AngularFireModule.initializeApp(environment.firebase),
		AngularFirestoreModule,
		AngularFireAuthModule,
		BrowserAnimationsModule,
		ToasterModule,
		TooltipModule,
		FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
		Ng2FileInputModule.forRoot()
	],
	providers: [AuthService, LoaderService, AuthGuard, CommonHelperService],
	bootstrap: [AppComponent]
})
export class AppModule { }
