import { Component, OnInit } from '@angular/core';
import { Ng2FileInputService } from 'ng2-file-input/dist/src/ng2-file-input.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';

@Component({
	selector: 'app-upload',
	templateUrl: './upload.component.html',
	styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

	constructor(
		private afs: AngularFirestore,
		private ng2FileInputService: Ng2FileInputService
	) { }

	ngOnInit() {
	}

	onAction(event) {
		console.log('Event :', event);
		console.log(this.ng2FileInputService.getCurrentFiles(event.id));

		const file = this.ng2FileInputService.getCurrentFiles(event.id);

		const fileRef = this.afs.app.storage().ref();

		fileRef.child('file.pdf').put(file[0]).then((snapshot) => {
			console.log('snapshot : ', snapshot);
			console.log('Uploaded a blob or file!');
		  })
		  .catch((error) => {
			  console.log('Error : ', error);
		  });
	}

}
