import { Component, OnInit, NgZone } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { ToasterService } from 'angular2-toaster';

import { LoaderService } from '../_services/loader.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../_services/providers/auth.service';
import { Http } from '@angular/http';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	user: any = {};
	isLoading = false;

	constructor(
		private _zone: NgZone,
		private angularFireAuth: AngularFireAuth,
		private toasterService: ToasterService,
		private loaderService: LoaderService,
		private router: Router,
		private authService: AuthService,
		private activatedRoute: ActivatedRoute,
		private http: Http
	) {
		this.loaderService.isLoading.subscribe(isLoading => {
			this.isLoading = isLoading;
		});
	}

	ngOnInit() {
		/** Example of File handling in Angular 5 */
		// this.http.get('assets/file.txt').subscribe((data) => {
		// 	const arr = ['tata', 'tutu', 'baba'];
		// 	console.log('data : ', data.text().split(' '));
		// 	const file = data.text().split(' ');
			// arr.every((word) => {
			// 	file.every((item) => {
			// 		if (word === item) {
			// 			console.log('true');
			// 			// return true;
			// 			throw '';
			// 		}
			// 	});
			// });

			// for (const word of arr) {
			// 	for (const item of file) {
			// 		if (word === item) {
			// 			console.log('true');
			// 			break;
			// 		}
			// 	}
			// }
		// });
	}

}
