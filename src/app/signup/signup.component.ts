import { Component, OnInit } from '@angular/core';

import { LoaderService } from '../_services/loader.service';
import { AuthService } from '../_services/providers/auth.service';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
	user: any = {};
	isLoading = false;

	constructor(
		private loaderService: LoaderService,
		private authService: AuthService
	) {
		this.loaderService.isLoading.subscribe(isLoading => {
			this.isLoading = isLoading;
		});
	}

	ngOnInit() {
	}

}
